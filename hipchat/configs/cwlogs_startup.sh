#!/bin/bash

# install and setup agent
# python ./awslogs-agent-setup.py -n -r $REGION -c $CONFIGS_S3_URL

# launch agent
#./awslogs-agent-launcher.sh

# the new way ?

# copy config file down
aws s3 cp $CONFIGS_S3_URL $AGENT_INSTALL/awslogs.conf

# run log forwarder in foregound
AWS_CONFIG_FILE=$AGENT_INSTALL/aws.conf aws logs push --config-file $AGENT_INSTALL/awslogs.conf
