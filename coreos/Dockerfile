FROM ubuntu
 
MAINTAINER Tom Jackson thomas.jackson@nordstrom.com

# Define env variables that will allow all containers to navigate the 
# Nordstrom outbound proxy .
# You only need these if you are building on-prem.
# Make sure to comment out before building on Docker Hub.
# ENV http_proxy http://pbcld-proxy.nordstrom.net:3128
# ENV https_proxy https://pbcld-proxy.nordstrom.net:3128 
# ENV no_proxy localhost,10.0.0/8,nordstrom.net

# A required input is the configuration file defining the logs you want aggregated. See exmaple
# in congis/awslogs-example.conf for an example
# The conf file is typically specified as an S3 URL, e.g. 
# s3://<bucket name>/<path to file>
ENV CONFIGS_S3_URL TBD

# Another input is the region your cloud watch logs group lives in
ENV REGION us-west-2

RUN apt-get update
RUN apt-get -y install wget
RUN apt-get -y install python

# Agent installation directory
ENV AGENT_INSTALL /var/awslogs/bin

# Add the shell script that runs the agent config script then launches the agent
ADD configs/cwlogs_startup.sh $AGENT_INSTALL/cwlogs_startup.sh
RUN chmod 0777 $AGENT_INSTALL/cwlogs_startup.sh

WORKDIR $AGENT_INSTALL

# get agent setup script
RUN wget https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py

# Fire up agent
CMD ["./cwlogs_startup.sh"] 
