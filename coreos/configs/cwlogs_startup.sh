#!/bin/bash

# install and setup agent
python ./awslogs-agent-setup.py -n -r $REGION -c $CONFIGS_S3_URL

# launch agent
./awslogs-agent-launcher.sh