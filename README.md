# Cloudwatch Logs

A Docker container that installs a Cloudwatch Logs Agent

# Requirements

Must be run on an EC2 host because only EC2 hosts can write to Cloud Watch Logs.

The EC2 host should be configured with an IAM role that has read permissions on the configuration file (defining what logs to forward)
and write permissions to the Cloud Watch Logs groups and streams specified in the configuration file.

# How Should I Run This?

Set the CONFIGS_S3_URL env variable to point to a configuration file, set REGION, then fire up the container. 
The CONFIGS_S3_URL must be formatted as an URL supported by cloud watch (see Amazon docs for supported formats)

The REGION env variable defines the AWS region where the Cloud Watch Log Group lives.

A sample container run is:

```
sudo docker run -d -e CONFIGS_S3_URL=s3://tts-sets-team/myapp/dev/myapp-cwlogs.conf -e REGION=us-west-2 --name myapp -it nordstromsets/cwlogs:coreos
```
# Log Volumes

Typically the configured log files will live on host volumes that need to mapped to volumes in the logs container.

# Troubleshooting

## Agent logs

For the hipchat version of the container, agent logs to stdout and can thus be viewed using the docker logs command (against container name)

For the coreos version of the container, the agent logs to /var/log/awslogs.log

## Can't Read Config File

Logs contain errors that suggest agent cannot read the file referenced in CONFIGS_S3_URL.

Attach to logs container, and verify the container is associated with the expected IAM role by running the following command:

```
curl -L http://169.254.169.254/latest/meta-data/iam/security-credential
```
